import { Injectable } from '@angular/core';
import { TimeTrackingEntry } from './timeTrackingEntry';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class TimeTrackingService {
  private url = 'http://lowcost-env.tnpt2ehfpq.eu-central-1.elasticbeanstalk.com/';
  private urlPostfixGetAllRecords = 'records';

  constructor(private http: Http) { }

  timeTrackingEntryList: TimeTrackingEntry[] = [
    new TimeTrackingEntry('Projet 1', "1 Start" , "1 Ende"),
    new TimeTrackingEntry('Projet 2', "2 Start" , "2 Ende")
  ];

  getTest(): Promise<string> {
      return Promise.resolve('XXX');
  }

  getTimeTrackingList() : Promise<TimeTrackingEntry[]> {
    return this.http.get(this.url + this.urlPostfixGetAllRecords).toPromise().then(response => response.json().Items as TimeTrackingEntry[]).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
