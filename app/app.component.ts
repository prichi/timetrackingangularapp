import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

import { TimeTrackingService } from './timeTracking.service';
import { TimeTrackingEntry } from './timeTrackingEntry';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: './Views/app.component.html'
})
export class AppComponent implements OnInit { 
  
  constructor( private timeTrackingService: TimeTrackingService) { }

  test: string;
  timeTrackingList: TimeTrackingEntry[];

  model: TimeTrackingEntry;

  onAddNewProjectButton() {
    console.log("Button clicked");
  }

  ngOnInit(): void {
    this.timeTrackingService.getTest().then(test => this.test = test);
    this.timeTrackingService.getTimeTrackingList().then(timeTrackingList => this.timeTrackingList = timeTrackingList);
  }
}
