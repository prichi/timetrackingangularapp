
export class TimeTrackingEntry {
    constructor(
        public ProjectName: string,
        public Start: string,
        public End: string) { }
}